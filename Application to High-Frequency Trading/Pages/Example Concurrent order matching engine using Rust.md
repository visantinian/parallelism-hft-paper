### Example: Concurrent Order Matching Engine Using Rust

An order matching engine is a core component of any trading system. It matches buy and sell orders to facilitate trades. Implementing a concurrent order matching engine in Rust involves handling multiple incoming orders concurrently and matching them efficiently.

#### Key Components

1. **Order Book**: Maintains buy and sell orders.
2. **Order Matching Logic**: Matches buy and sell orders based on price and time priority.
3. **Concurrency Management**: Handles multiple incoming orders concurrently.

#### Implementation

**Step 1: Define Order and Order Book Structures**

```rust
use std::collections::VecDeque;
use std::sync::{Arc, Mutex};
use std::thread;

#[derive(Debug, Clone)]
enum OrderType {
    Buy,
    Sell,
}

#[derive(Debug, Clone)]
struct Order {
    id: usize,
    order_type: OrderType,
    price: f64,
    quantity: usize,
}

struct OrderBook {
    buy_orders: VecDeque<Order>,
    sell_orders: VecDeque<Order>,
}

impl OrderBook {
    fn new() -> Self {
        OrderBook {
            buy_orders: VecDeque::new(),
            sell_orders: VecDeque::new(),
        }
    }

    fn add_order(&mut self, order: Order) {
        match order.order_type {
            OrderType::Buy => self.buy_orders.push_back(order),
            OrderType::Sell => self.sell_orders.push_back(order),
        }
    }

    fn match_orders(&mut self) {
        while let (Some(buy_order), Some(sell_order)) = (self.buy_orders.front(), self.sell_orders.front()) {
            if buy_order.price >= sell_order.price {
                let trade_quantity = buy_order.quantity.min(sell_order.quantity);
                println!(
                    "Matched Order: Buy Order {:?} with Sell Order {:?} for quantity {}",
                    buy_order, sell_order, trade_quantity
                );

                if buy_order.quantity > sell_order.quantity {
                    let mut updated_buy_order = buy_order.clone();
                    updated_buy_order.quantity -= trade_quantity;
                    self.buy_orders.pop_front();
                    self.buy_orders.push_front(updated_buy_order);
                    self.sell_orders.pop_front();
                } else if sell_order.quantity > buy_order.quantity {
                    let mut updated_sell_order = sell_order.clone();
                    updated_sell_order.quantity -= trade_quantity;
                    self.sell_orders.pop_front();
                    self.sell_orders.push_front(updated_sell_order);
                    self.buy_orders.pop_front();
                } else {
                    self.buy_orders.pop_front();
                    self.sell_orders.pop_front();
                }
            } else {
                break;
            }
        }
    }
}
```

**Step 2: Concurrent Order Processing**

We will use Rust's threading capabilities to handle multiple incoming orders concurrently.

```rust
fn main() {
    let order_book = Arc::new(Mutex::new(OrderBook::new()));
    
    let orders = vec![
        Order { id: 1, order_type: OrderType::Buy, price: 100.0, quantity: 10 },
        Order { id: 2, order_type: OrderType::Sell, price: 90.0, quantity: 5 },
        Order { id: 3, order_type: OrderType::Buy, price: 105.0, quantity: 7 },
        Order { id: 4, order_type: OrderType::Sell, price: 95.0, quantity: 8 },
        Order { id: 5, order_type: OrderType::Buy, price: 102.0, quantity: 12 },
    ];

    let mut handles = Vec::new();

    for order in orders {
        let order_book = Arc::clone(&order_book);
        let handle = thread::spawn(move || {
            let mut order_book = order_book.lock().unwrap();
            order_book.add_order(order.clone());
            order_book.match_orders();
        });
        handles.push(handle);
    }

    for handle in handles {
        handle.join().unwrap();
    }

    let final_order_book = order_book.lock().unwrap();
    println!("Final Order Book: {:?}", *final_order_book);
}
```

#### Explanation

1. **Order and Order Book Structures**:
    - Define `Order` and `OrderBook` structures to store and manage orders.
    - The `OrderBook` has methods to add orders and match them.

2. **Adding and Matching Orders**:
    - The `add_order` method adds an order to the appropriate queue (buy or sell).
    - The `match_orders` method matches buy and sell orders based on price and quantity.

3. **Concurrent Order Processing**:
    - Use Rust's `thread` module to spawn threads for processing each order.
    - Each thread locks the `OrderBook` using a `Mutex`, adds an order, and attempts to match orders.

4. **Shared Order Book**:
    - Use `Arc` to share the `OrderBook` instance across threads safely.
    - Use `Mutex` to ensure thread-safe access to the `OrderBook`.

5. **Final Order Book**:
    - After all threads have completed, print the final state of the order book.

#### Running the Code

To run the code, add the following dependencies to your `Cargo.toml`:

```toml
[dependencies]
```

Then compile and run your Rust project:

```sh
cargo run
```

You should see the matched orders and the final state of the order book printed to the console.

### Conclusion

This example demonstrates how to implement a concurrent order matching engine in Rust. By leveraging Rust's concurrency features, such as threading and synchronization primitives (`Arc` and `Mutex`), we can handle multiple incoming orders concurrently and match them efficiently. This approach ensures that the order matching engine can process high volumes of trades with minimal latency, making it suitable for high-frequency trading applications.