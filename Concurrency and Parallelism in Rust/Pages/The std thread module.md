#### Overview

The `std::thread` module in Rust provides the building blocks for creating and managing threads. Threads are units of execution within a program, allowing multiple operations to be performed concurrently. Rust’s thread module ensures safe concurrent programming by leveraging its ownership and type system to prevent common concurrency issues such as data races and deadlocks.

#### Key Features of the `std::thread` Module

1. **Creating Threads**: The primary function for creating a new thread is `thread::spawn`, which takes a closure and executes it in a new thread.
2. **Joining Threads**: The `join` method allows the main thread to wait for the completion of a spawned thread, ensuring synchronization between threads.
3. **Thread Handles**: When a thread is spawned, it returns a `JoinHandle`, which can be used to interact with the thread, such as waiting for it to finish.
4. **Scoped Threads**: Using external crates like `crossbeam`, Rust can create scoped threads that ensure all threads are joined before a scope ends, preventing dangling threads.

#### Example: Basic Thread Creation

**Creating and Joining Threads:**
```rust
use std::thread;
use std::time::Duration;

fn main() {
    let handle = thread::spawn(|| {
        for i in 1..10 {
            println!("Hello from the spawned thread: {}", i);
            thread::sleep(Duration::from_millis(1));
        }
    });

    for i in 1..5 {
        println!("Hello from the main thread: {}", i);
        thread::sleep(Duration::from_millis(1));
    }

    handle.join().unwrap();
}
```
In this example:
- `thread::spawn` creates a new thread to run the closure.
- The `main` thread continues to run concurrently.
- `handle.join()` waits for the spawned thread to complete before exiting the program.

#### Example: Thread Safety with Mutex

To safely share data between threads, Rust provides synchronization primitives like `Mutex` and `Arc` (Atomic Reference Counting).

**Sharing Data with Mutex:**
```rust
use std::sync::{Arc, Mutex};
use std::thread;

fn main() {
    let counter = Arc::new(Mutex::new(0));
    let mut handles = vec![];

    for _ in 0..10 {
        let counter = Arc::clone(&counter);
        let handle = thread::spawn(move || {
            let mut num = counter.lock().unwrap();
            *num += 1;
        });
        handles.push(handle);
    }

    for handle in handles {
        handle.join().unwrap();
    }

    println!("Result: {}", *counter.lock().unwrap());
}
```
In this example:
- `Mutex` is used to ensure that only one thread can modify the counter at a time.
- `Arc` allows multiple threads to own the same `Mutex` safely.
- Each thread locks the mutex, increments the counter, and then releases the lock.

#### Example: Using Channels for Communication

Rust’s `std::sync::mpsc` module provides channels for message passing between threads. `mpsc` stands for multiple producer, single consumer.

**Sending and Receiving Messages:**
```rust
use std::sync::mpsc;
use std::thread;
use std::time::Duration;

fn main() {
    let (tx, rx) = mpsc::channel();

    thread::spawn(move || {
        let vals = vec![
            String::from("hi"),
            String::from("from"),
            String::from("the"),
            String::from("thread"),
        ];

        for val in vals {
            tx.send(val).unwrap();
            thread::sleep(Duration::from_millis(1));
        }
    });

    for received in rx {
        println!("Got: {}", received);
    }
}
```
In this example:
- A channel is created using `mpsc::channel()`.
- The sender (`tx`) is moved to the spawned thread, which sends messages.
- The receiver (`rx`) iterates over the received messages in the main thread.

#### Scoped Threads with Crossbeam

For more complex threading needs, the `crossbeam` crate provides scoped threads, ensuring all threads are joined before the scope ends.

**Scoped Threads Example:**
```rust
use crossbeam::thread;

fn main() {
    let mut data = vec![1, 2, 3, 4];

    thread::scope(|s| {
        for i in &mut data {
            s.spawn(move |_| {
                *i += 1;
            });
        }
    }).unwrap();

    println!("{:?}", data);
}
```
In this example:
- `thread::scope` creates a scope for spawning threads.
- Threads can borrow local variables, ensuring they are joined before the scope ends, preventing dangling references.

#### Conclusion

The `std::thread` module in Rust provides essential tools for creating and managing threads, allowing for safe concurrent programming. With features like thread creation, joining, synchronization primitives, and message passing, Rust ensures that developers can write efficient and safe concurrent code. By leveraging Rust’s ownership and type system, these concurrency primitives help prevent common bugs and ensure robust multi-threaded applications.