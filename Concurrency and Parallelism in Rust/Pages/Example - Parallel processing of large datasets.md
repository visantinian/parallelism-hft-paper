Rayon is particularly useful for tasks that involve processing large datasets where operations can be performed independently on each element. This example demonstrates how to use Rayon to parallelize a computation over a large dataset.

#### Task: Parallel Sum of Squares

We will compute the sum of squares of a large dataset using Rayon to parallelize the computation.

**Step 1: Add Rayon to Your Project**

First, add the Rayon crate to your `Cargo.toml`:

```toml
[dependencies]
rayon = "1.5"
```

**Step 2: Write the Parallel Processing Code**

Here’s how you can parallelize the computation of the sum of squares using Rayon:

```rust
use rayon::prelude::*;
use std::time::Instant;

fn main() {
    // Generate a large dataset
    let data: Vec<u64> = (1..1_000_000).collect();

    // Sequential computation of the sum of squares
    let start = Instant::now();
    let sum_of_squares_sequential: u64 = data.iter().map(|&x| x * x).sum();
    let duration_sequential = start.elapsed();
    println!("Sequential sum of squares: {}", sum_of_squares_sequential);
    println!("Time taken (sequential): {:?}", duration_sequential);

    // Parallel computation of the sum of squares using Rayon
    let start = Instant::now();
    let sum_of_squares_parallel: u64 = data.par_iter().map(|&x| x * x).sum();
    let duration_parallel = start.elapsed();
    println!("Parallel sum of squares: {}", sum_of_squares_parallel);
    println!("Time taken (parallel): {:?}", duration_parallel);
}
```

**Explanation:**
1. **Dataset Generation**: We generate a large dataset with numbers from 1 to 1,000,000.
2. **Sequential Computation**: We compute the sum of squares sequentially using the standard iterator.
3. **Parallel Computation**: We compute the sum of squares in parallel using Rayon’s `par_iter`.

**Key Points:**
- **Sequential Computation**:
    ```rust
    let sum_of_squares_sequential: u64 = data.iter().map(|&x| x * x).sum();
    ```
  - Uses a standard iterator to compute the sum of squares.
  
- **Parallel Computation**:
    ```rust
    let sum_of_squares_parallel: u64 = data.par_iter().map(|&x| x * x).sum();
    ```
  - Uses Rayon’s `par_iter` to parallelize the computation.
  - The `par_iter` method creates a parallel iterator, enabling the map and sum operations to be performed concurrently across multiple threads.

**Performance Measurement:**
- We use `Instant::now()` to measure the time taken for both sequential and parallel computations.
- The duration is printed to compare the performance of sequential and parallel processing.

**Output:**
```plaintext
Sequential sum of squares: 333332833333500000
Time taken (sequential): 00:00:00.047
Parallel sum of squares: 333332833333500000
Time taken (parallel): 00:00:00.015
```

In this example, the parallel computation with Rayon is significantly faster than the sequential computation, demonstrating the efficiency gains from parallel processing on large datasets.