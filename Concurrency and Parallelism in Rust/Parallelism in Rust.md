
[[The Rayon library for data parallelism]]
[[Example - Parallel processing of large datasets]]

### Parallelism in Rust

Parallelism in Rust involves executing multiple tasks simultaneously to improve performance, particularly for CPU-bound operations. Rust's powerful abstractions and libraries make it straightforward to write parallel code while ensuring safety and performance.

#### Key Concepts of Parallelism in Rust

1. **Task Parallelism**: Running multiple tasks or functions in parallel.
2. **Work Stealing**: Efficiently balancing the workload across threads to avoid idle cores.

#### Task Parallelism with Threads

Rust's standard library supports task parallelism using threads. The `std::thread` module can be used for running different tasks in parallel.

**Example: Parallel Tasks with Threads**
```rust
use std::thread;

fn main() {
    let handles: Vec<_> = (0..10).map(|i| {
        thread::spawn(move || {
            println!("Task {} is running", i);
        })
    }).collect();

    for handle in handles {
        handle.join().unwrap();
    }
}
```

In this example:
- **Thread Creation**: Spawns 10 threads, each running a simple task.
- **Joining Threads**: Waits for all threads to complete before exiting the program.

#### Communication Between Threads with Channels

Rust’s `std::sync::mpsc` module provides channels for message passing between threads. `mpsc` stands for multiple producer, single consumer.

**Example: Sending and Receiving Messages with Channels**
```rust
use std::sync::mpsc;
use std::thread;
use std::time::Duration;

fn main() {
    let (tx, rx) = mpsc::channel();

    thread::spawn(move || {
        let vals = vec![
            String::from("hi"),
            String::from("from"),
            String::from("the"),
            String::from("thread"),
        ];

        for val in vals {
            tx.send(val).unwrap();
            thread::sleep(Duration::from_millis(1));
        }
    });

    for received in rx {
        println!("Got: {}", received);
    }
}
```

In this example:
- A channel is created using `mpsc::channel()`.
- The sender (`tx`) is moved to the spawned thread, which sends messages.
- The receiver (`rx`) iterates over the received messages in the main thread.

#### Work Stealing with Crossbeam

For more complex threading needs, the `crossbeam` crate provides scoped threads and work-stealing capabilities, ensuring efficient task distribution and joining.

**Example: Scoped Threads with Crossbeam**
```rust
use crossbeam::thread;

fn main() {
    let mut data = vec![1, 2, 3, 4];

    thread::scope(|s| {
        for i in &mut data {
            s.spawn(move |_| {
                *i += 1;
            });
        }
    }).unwrap();

    println!("{:?}", data);
}
```

In this example:
- **Scoped Threads**: `thread::scope` ensures all threads are joined before the scope ends, preventing dangling threads.
- **Mutating Data**: Each thread mutates the shared data safely within the scope.

#### Asynchronous Programming with `async` and `await`

Rust’s `async` and `await` keywords simplify writing asynchronous code. Asynchronous programming allows tasks to run concurrently without blocking the main thread, making it ideal for I/O-bound operations.

**Example: Asynchronous Network Request**
```rust
use reqwest;
use tokio;

#[tokio::main]
async fn main() -> Result<(), reqwest::Error> {
    let response = reqwest::get("https://www.rust-lang.org").await?;
    let body = response.text().await?;
    println!("Body: {}", body);
    Ok(())
}
```

In this example:
- The `#[tokio::main]` attribute sets up a Tokio runtime, allowing the `main` function to be asynchronous.
- `reqwest::get("https://www.rust-lang.org").await` makes an asynchronous HTTP GET request.
- `response.text().await?` awaits the response body, handling it asynchronously.

#### Parallel Sorting

Rust provides built-in support for parallel sorting, which can significantly speed up sorting operations on large datasets.

**Example: Parallel Sorting**
```rust
use std::thread;

fn parallel_sort<T: Ord + Send + 'static>(v: &mut [T]) {
    let len = v.len();
    if len <= 1 {
        return;
    }

    let mid = len / 2;
    let (left, right) = v.split_at_mut(mid);

    let left_handle = thread::spawn(move || parallel_sort(left));
    let right_handle = thread::spawn(move || parallel_sort(right));

    left_handle.join().unwrap();
    right_handle.join().unwrap();

    let mut sorted = v.to_vec();
    merge_sorted(&mut sorted, left, right);
    v.copy_from_slice(&sorted);
}

fn merge_sorted<T: Ord + Copy>(result: &mut [T], left: &[T], right: &[T]) {
    let mut i = 0;
    let mut j = 0;
    for k in 0..result.len() {
        if i >= left.len() {
            result[k] = right[j];
            j += 1;
        } else if j >= right.len() {
            result[k] = left[i];
            i += 1;
        } else if left[i] <= right[j] {
            result[k] = left[i];
            i += 1;
        } else {
            result[k] = right[j];
            j += 1;
        }
    }
}

fn main() {
    let mut numbers: Vec<i32> = (1..1_000_000).rev().collect();
    parallel_sort(&mut numbers);
    println!("Parallel sorting completed");
}
```

In this example:
- **Parallel Sorting**: Uses threads to sort the left and right halves of the array in parallel.
- **Merge Function**: Merges the sorted halves back together.

#### GPU Programming with Rust

For tasks that require massive parallelism, Rust can also be used for GPU programming. While this is more advanced and requires additional libraries, it enables leveraging the computational power of GPUs.

**Example: Basic GPU Programming Setup**
```rust
// Example of integrating Rust with CUDA (Note: Requires additional setup and libraries)

use rustacuda::prelude::*;
use rustacuda::memory::DeviceBuffer;

fn main() -> Result<(), Box<dyn std::error::Error>> {
    rustacuda::init(CudaFlags::empty())?;
    let device = Device::get_device(0)?;
    let context = Context::create_and_push(ContextFlags::MAP_HOST | ContextFlags::SCHED_AUTO, device)?;

    let mut device_buffer = DeviceBuffer::from_slice(&[1.0f32; 1000])?;

    // Launch kernel, etc.

    Ok(())
}
```

This example demonstrates the initial setup for integrating Rust with CUDA for GPU programming, enabling high-performance parallel computations.

#### Conclusion

Parallelism in Rust is facilitated by powerful abstractions and libraries, including the standard thread module, crossbeam for scoped threads and work-stealing, and async/await for asynchronous tasks. Whether you need task parallelism for running different tasks concurrently or parallel sorting for large datasets, Rust provides the tools to write efficient and safe parallel code. For even greater parallelism, Rust can interface with GPUs, unlocking massive computational power for demanding applications.