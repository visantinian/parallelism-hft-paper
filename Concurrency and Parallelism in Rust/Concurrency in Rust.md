
[[The std thread module]]
[[The `async` and `await` keywords]]
[[Example - Building a concurrent web server]]

Concurrency in Rust is made safe and efficient through its ownership model, type system, and various concurrency primitives. Rust's design ensures that common concurrency bugs, such as data races, are caught at compile time, making concurrent programming more reliable.

#### Key Concepts of Concurrency in Rust

1. **Ownership and Borrowing**:
   - Rust’s ownership model ensures that data has a single owner at a time, preventing concurrent modification.
   - Borrowing rules allow for safe references to data, with the compiler enforcing these rules to prevent data races.

2. **Thread Safety**:
   - Rust uses the `Send` and `Sync` traits to ensure thread safety.
   - `Send` allows transferring ownership of types across threads.
   - `Sync` allows types to be safely referenced from multiple threads.

3. **Concurrency Primitives**:
   - Rust provides various primitives for handling concurrency, such as threads, mutexes, and channels.

#### Creating and Managing Threads

The `std::thread` module provides the basic building blocks for creating and managing threads in Rust.

**Example: Basic Thread Creation and Joining**
```rust
use std::thread;
use std::time::Duration;

fn main() {
    let handle = thread::spawn(|| {
        for i in 1..10 {
            println!("Hello from the spawned thread: {}", i);
            thread::sleep(Duration::from_millis(1));
        }
    });

    for i in 1..5 {
        println!("Hello from the main thread: {}", i);
        thread::sleep(Duration::from_millis(1));
    }

    handle.join().unwrap();
}
```
In this example:
- `thread::spawn` creates a new thread to run the closure.
- The main thread continues to run concurrently.
- `handle.join()` waits for the spawned thread to complete before exiting the program.

#### Safe Shared State with Mutex

To safely share data between threads, Rust provides the `Mutex` type, which ensures that only one thread can access the data at a time.

**Example: Using Mutex for Shared State**
```rust
use std::sync::{Arc, Mutex};
use std::thread;

fn main() {
    let counter = Arc::new(Mutex::new(0));
    let mut handles = vec![];

    for _ in 0..10 {
        let counter = Arc::clone(&counter);
        let handle = thread::spawn(move || {
            let mut num = counter.lock().unwrap();
            *num += 1;
        });
        handles.push(handle);
    }

    for handle in handles {
        handle.join().unwrap();
    }

    println!("Result: {}", *counter.lock().unwrap());
}
```
In this example:
- `Mutex` is used to ensure mutual exclusion, so only one thread can increment the counter at a time.
- `Arc` (Atomic Reference Counting) allows multiple threads to share ownership of the `Mutex`.

#### Communication Between Threads with Channels

Rust’s `std::sync::mpsc` module provides channels for message passing between threads. `mpsc` stands for multiple producer, single consumer.

**Example: Sending and Receiving Messages with Channels**
```rust
use std::sync::mpsc;
use std::thread;
use std::time::Duration;

fn main() {
    let (tx, rx) = mpsc::channel();

    thread::spawn(move || {
        let vals = vec![
            String::from("hi"),
            String::from("from"),
            String::from("the"),
            String::from("thread"),
        ];

        for val in vals {
            tx.send(val).unwrap();
            thread::sleep(Duration::from_millis(1));
        }
    });

    for received in rx {
        println!("Got: {}", received);
    }
}
```
In this example:
- A channel is created using `mpsc::channel()`.
- The sender (`tx`) is moved to the spawned thread, which sends messages.
- The receiver (`rx`) iterates over the received messages in the main thread.

#### Asynchronous Programming with `async` and `await`

Rust’s `async` and `await` keywords simplify writing asynchronous code. Asynchronous programming allows tasks to run concurrently without blocking the main thread, making it ideal for I/O-bound operations.

**Example: Asynchronous Network Request**
```rust
use reqwest;
use tokio;

#[tokio::main]
async fn main() -> Result<(), reqwest::Error> {
    let response = reqwest::get("https://www.rust-lang.org").await?;
    let body = response.text().await?;
    println!("Body: {}", body);
    Ok(())
}
```
In this example:
- The `#[tokio::main]` attribute sets up a Tokio runtime, allowing the `main` function to be asynchronous.
- `reqwest::get("https://www.rust-lang.org").await` makes an asynchronous HTTP GET request.
- `response.text().await?` awaits the response body, handling it asynchronously.

#### Scoped Threads with Crossbeam

For more complex threading needs, the `crossbeam` crate provides scoped threads, ensuring all threads are joined before the scope ends.

**Example: Scoped Threads**
```rust
use crossbeam::thread;

fn main() {
    let mut data = vec![1, 2, 3, 4];

    thread::scope(|s| {
        for i in &mut data {
            s.spawn(move |_| {
                *i += 1;
            });
        }
    }).unwrap();

    println!("{:?}", data);
}
```
In this example:
- `thread::scope` creates a scope for spawning threads.
- Threads can borrow local variables, ensuring they are joined before the scope ends, preventing dangling references.

#### Conclusion

Concurrency in Rust is made safe and efficient through its ownership model, type system, and various concurrency primitives. By using threads, mutexes, channels, and asynchronous programming, Rust ensures that developers can write concurrent code that is both performant and free from common concurrency bugs.